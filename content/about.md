+++
title = "About Me"
slug = "about"
+++

Howdy! My name is Jonathan Saenz.


<!-- My goal to create content related to my personal interests, education, and personal growth. I will try to use platforms such as 
this site, my blog, and youtube to publish my content. -->


I am currently a computer engineer at Texas A&M and my area of interest lies in cybersecurity, software engineering, and DevOps technologies.
I am graduating May 2022 and during my remaining time at university, I am actively trying to grow Texas A&M Cybersecurity Club and TAMU Red Hat Academy program with the leadership roles I hold in those organizations.

<p style="margin-top:30px;text-align:center;">
  <img src="/images/cyberClubLogo.png" width="350" style="margin-right:10px;"/>
  <img src="/images/rhaLogo.png" width="235" style="margin-left:10px;"/> 
</p>

<!-- # Next Generation Empowerment

I am also the founder and president of a non-profit called Next Generation Empowerment. It was founded in Novemeber 2020 and the mission statement is below.
> Our mission is to empower the younger generation to find a sense of purpose, build confidence, and grow as leaders by providing assistance to receive various opportunities to build their career. We aim for this mission to become a movement so the current generation that has been impacted by our efforts will have the same desire to help the next generation in hopes of having an everlasting generational impact.


I know I would not be in the position I am today if it were not for the help of others. I will always feel indebted because of that and will do my part to help the people I can reach. I hope to use my knowledge, skills, and experience in any capacity to help motivate, inspire, and impact someone else's life. Check out nextgenempower tag in my blog to see our progress and any updates. -->